package planets;
import java.util.*;
	/**
	 * @author Juan-Carlos Sreng-Flores
	 */
public class CollectionMethods {
	/**
	 * @param planets - Collection<Planet> as input.
	 * @return Collection<Planet> as return of all the planets within an order of 3;
	 * This method collects all the planets within an order of 3.
	 */
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		Collection<Planet> innerPlanets = new ArrayList<Planet>();
		for(Planet planet : planets) {
			if(planet.getOrder() >= 1 && planet.getOrder()<=3) {
				innerPlanets.add(planet);
			}
		}
		return innerPlanets;
	}
}
