package planets;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	/**
	 * This method overrides the equals method from Object class.
	 * @param Object o the object to be compared with.
	 * @return boolean the boolean value if the two objects are equal or not.
	 */
	@Override
	public boolean equals(Object o) {
		boolean isEq = false;
		if(o instanceof Planet) {
			Planet p  = (Planet)o;
			isEq = this.name.equals(p.name);
			isEq = isEq && this.order == p.order;
		}
		return isEq;
	}
	
	/**
	 * @return int - returns hascode value of a Planet Object.
	 */
	@Override
	public int hashCode() {
		String hash = this.name+this.order;
		return hash.hashCode();
	}
	
	/**
	 * @param planet - Planet object to be compared with.
	 * @return int - comparison integer value.
	 */
	public int compareTo(Planet planet) {
		int compareName = this.name.compareTo(planet.name);
		if(compareName == 0) {
			compareName = planet.planetarySystemName.compareTo(this.planetarySystemName);
		}
		return compareName;
	}
}
