package employee;
/**
 * This class creates an Object Type HourlyEmployee where
 * it implements the Employee Interface through getYearlyPay() method. 
 * It stores in two attributes, weeklyHoursWorked and hourlyPay;
 * @author Juan-Carlos Sreng-Flores
 *
 */
public class HourlyEmployee implements Employee{
	private double hourlyPay;		//private attribute, pay in hours.
	private int weeklyHoursWorked; 	//private attribute number of hours worked.
	/**
	 * This constructor takes in as argument hourlyPay and weeklyHoursWorked
	 * in order to create an Object Type Hourly Employee.
	 * @param weeklyHoursWorked
	 * @param hourlyPay
	 */
	public HourlyEmployee(double hourlyPay, int weeklyHoursWorked) {
		this.weeklyHoursWorked = weeklyHoursWorked;
		this.hourlyPay = hourlyPay;
	}
	/**
	 * Computes the salary of the whole year from an HourlyEmployee
	 * @return int
	 */
	public int getYearlyPay() {
		return (int)(hourlyPay*weeklyHoursWorked*52); //hourpay*weeklyHour*weeks.
	}
}
