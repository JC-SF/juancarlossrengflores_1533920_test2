package employee;

/**
 * Employee Interface. Classes that implements it
 * need to implement getYealyPay method.
 * @author Juan-Carlos Sreng-Flores
 *
 */
public interface Employee {
	/**
	 * getYealyPay to be implemented in classes.
	 * @return int the yearlySalary.
	 */
	int getYearlyPay();
}
