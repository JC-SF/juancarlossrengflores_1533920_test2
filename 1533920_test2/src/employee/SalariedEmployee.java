package employee;
/**
 * @author Juan-Carlos Sreng-Flores
 * Salaried Employee Object type that implements interface Employee.
 */
public class SalariedEmployee implements Employee{
	private int salary; //Salaried Employee's yearly salary
	/**
	 * Constructor method that takes salary as argument and stores in private field.
	 * @param salary
	 */
	public SalariedEmployee(int salary) {
		this.salary = salary;
	}
	/**
	 * Implemented method from Interface. Returns the yearly salary of the 
	 * Employee.
	 * @return int the yearlySalary.
	 */
	public int getYearlyPay() {
		return this.salary;
	}
}
