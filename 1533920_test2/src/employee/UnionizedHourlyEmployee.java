package employee;
/**
 * @author Juan-Carlos Sreng-Flores
 * Creates an Employee extedning the HourlyEmployee class and 
 * implements the Employee interface.
 */
public class UnionizedHourlyEmployee extends HourlyEmployee implements Employee{
	private int pensionContribution;
	public UnionizedHourlyEmployee(double hourlyPay, int weeklyHoursWorked, int pensionContribution) {
		super(hourlyPay, weeklyHoursWorked);
		this.pensionContribution = pensionContribution;
	}
	/**
	 * This method computes the same calculation as HourlyEmployee: week*hourPay*52weeks
	 * but adds also another factor which is pensionContribution.
	 * @return int - yearly salary of the employee
	 */
	@Override
	public int getYearlyPay() {
		return super.getYearlyPay()+pensionContribution;
	}
}
