package tests;
import planets.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
	/**
	 * 
	 * @author Juan-Carlos Sreng-Flores
	 *
	 */
class CollectionsMethodsTests {
	/**
	 * This method tests the getInnerPLanets from CollectionMethods class.
	 */
	@Test
	void test() {
		Collection<Planet> collection = new ArrayList<Planet>();
		collection.add(new Planet("Solar System", "Earth", 0, 50000));
		collection.add(new Planet("Solar System", "Venus", 1, 50000));
		collection.add(new Planet("Solar System", "Merc", 2, 50000));
		collection.add(new Planet("Solar Sm", "Mars", 7, 50000));	
		collection.add(new Planet("Solar System", "Earthy", 3, 50));
		collection.add(new Planet("Solar em", "Eth", 2, 5000));
		Collection<Planet> expected = new ArrayList<Planet>();
		expected.add(new Planet("Solar System", "Venus", 1, 50000));
		expected.add(new Planet("Solar System", "Merc", 2, 50000));
		expected.add(new Planet("Solar System", "Earthy", 3, 50));
		expected.add(new Planet("Solar em", "Eth", 2, 5000));
		collection = CollectionMethods.getInnerPlanets(collection);
		assertEquals(expected, collection);
	}

}
