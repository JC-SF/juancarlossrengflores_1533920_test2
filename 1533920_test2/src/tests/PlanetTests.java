package tests;
import planets.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
	/**
	 * Planet tests;
	 * @author Juan-Carlos Sreng-Flores
	 *
	 */
class PlanetTests {
	/**
	 * Test the planet overriden equals() method.
	 */
	@Test
	void testPlanetEquals() {
		Planet planet1 = new Planet("Solar System", "Earth", 3, 50000);
		Planet planet2 = new Planet("Not Solar System", "Earth", 3, 0);
		assertEquals(true, planet1.equals(planet2));
		planet1 = new Planet("Solar System", "Mars", 5, 500);
		planet2 = new Planet("Solar System", "Marsy", 5, 500);
		assertEquals(false, planet1.equals(planet2));
		planet1 = new Planet("Solar System", "Mars", 5, 500);
		planet2 = new Planet("Solar System", "Mars", 6, 500);
		assertEquals(false, planet1.equals(planet2));
		planet1 = new Planet("Solar System", "Mars", 5, 501);
		planet2 = new Planet("Solar System", "Mars", 6, 0);
		assertEquals(false, planet1.equals(planet2));
	}
	/**
	 * Tests the overriden hashCode() method from Planet class.
	 */
	@Test
	void testHashCode() {
		Planet planet1 = new Planet("Solar System", "Earth", 3, 50000);
		String hash = "Earth3";
		assertEquals(hash.hashCode(), planet1.hashCode());
	}
	/**
	 * Tests the implemented method from Planet class where needed to be implemented
	 * for the Comparable<T> Interface.
	 */
	@Test
	void testCompareTo() {
		Planet planet1 = new Planet("Solar System", "Earth", 3, 50000);
		Planet planet2 = new Planet("Not Solar System", "Earth", 3, 50000);
		assertEquals(true, planet1.compareTo(planet2)<0);
		planet1 = new Planet("Solar System", "Marsy", 5, 500);
		planet2 = new Planet("Solar System", "Mars", 5, 500);
		assertEquals(true, planet1.compareTo(planet2)>0);
		planet1 = new Planet("Solar System", "Mars", 5, 500);
		planet2 = new Planet("Solar System", "Mars", 6, 500);
		assertEquals(true, planet1.compareTo(planet2)==0);
		planet1 = new Planet("Solar Systemyyy", "Mars", 5, 501);
		planet2 = new Planet("Solar System", "Mars", 6, 0);
		assertEquals(true, planet1.compareTo(planet2)<0);
	}
	/**
	 * Test the arrays sort method.
	 */
	@Test 
	void testArraysSort() {
		String[] planets = new String[6];
		planets[0] = (new Planet("z", "a", 6, 500)).getName();
		planets[1] = (new Planet("aa", "aa", 6, 500)).getName();
		planets[2] = (new Planet("zz", "aa", 6, 500)).getName();
		planets[3] = (new Planet("b", "z", 6, 500)).getName();
		planets[4] = (new Planet("aa", "zz", 6, 500)).getName();
		planets[5] = (new Planet("s", "f", 6, 500)).getName();
		Arrays.sort(planets);
		String[] expected = new String[6];
		expected[0] = (new Planet("z", "a", 6, 500)).getName();
		expected[1] = (new Planet("zz", "aa", 6, 500)).getName();
		expected[2] = (new Planet("aa", "aa", 6, 500)).getName();
		expected[3] = (new Planet("s", "f", 6, 500)).getName();
		expected[4] = (new Planet("b", "z", 6, 500)).getName();
		expected[5] = (new Planet("aa", "zz", 6, 500)).getName();
		assertEquals(Arrays.toString(expected), Arrays.toString(planets));
	}
}
