package payroll;
import employee.*;
/**
 * @author Juan-Carlos Sreng-Flores
 * Creates an Object Type Payroll Management where it tracks
 * expenses from employees.
 *
 */
public class PayrollManagement {
	/**
	 * This method takes in as input Employee[] and adds all of their salaries.
	 * @param employees - Employee[] containing all the employees.
	 * @return int representing the totalSalaries of the employees.
	 */
	public static int getTotalExpenses(Employee[] employees) {
		int totalExpenses = 0;
		for(Employee emp : employees) {
			totalExpenses += emp.getYearlyPay();
		}
		return totalExpenses;
	}
	/**
	 * Main method, computed the total expenses from employees.
	 * @param args
	 */
	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(200000);
		employees[1] = new HourlyEmployee(13.5, 50);
		employees[2] = new UnionizedHourlyEmployee(13, 40, 5000);
		employees[3] = new UnionizedHourlyEmployee(30, 35, 3000);
		employees[4] = new HourlyEmployee(15, 40);
		System.out.println("Total expenses from Employees: "+getTotalExpenses(employees));
	}
}
